import { Component, OnInit } from '@angular/core';
import { Heroe } from '../../interfaces/heroes.interface';
import { HeroesService } from '../../services/heroes.service';

@Component({
  selector: 'app-buscar',
  templateUrl: './buscar.component.html',
  styleUrls: ['./buscar.component.css']
})
export class BuscarComponent implements OnInit {

  public termino: string = '';
  public heroes: Heroe[] = [];
  public heroeSeleccionado!: Heroe;
  public errorBusqueda: boolean = false;

  constructor(private servicio: HeroesService) { }

  ngOnInit(): void {
  }

  buscar(){
    this.errorBusqueda = false;
    this.servicio.getAutoCompletado(this.termino)
    .subscribe(resp =>{
      this.heroes = resp;
    })
  }
  opcionSeleccionada(evento: any){
    if(!evento.option.value){
      this.errorBusqueda = true;
      return;
    }
    const heroe: Heroe = evento.option.value;
    this.termino = heroe.superhero;
    this.servicio.getHeroe(heroe.id!)
    .subscribe(heroe => {this.heroeSeleccionado = heroe})
  }

}
